import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  posts: any;

  constructor(public navCtrl: NavController, public http: Http) {

    this.http.get('http://94.177.199.253:8000/apiEvents').map(res => res.json()).subscribe(data => {
        this.posts = data;
    });

  }
}

  class MyPage {
    @ViewChild(Slides) slides: Slides;

    goToSlide() {
      this.slides.slideTo(2, 500);
    }

}
