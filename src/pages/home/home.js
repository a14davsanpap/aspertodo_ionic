.factory('LoginService', function($http) {
    return {
     loginUser: function(user, password) {
         return $http.post('http://mydomain/login', {
               user: user,
               password: password
         });
     }
    };
