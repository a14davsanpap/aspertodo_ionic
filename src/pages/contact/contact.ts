import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

    posts: any;

  constructor(public navCtrl: NavController, public http: Http) {

  this.http.get('http://94.177.199.253:8000/apitas').map(res => res.json()).subscribe(data => {
      this.posts = data;
  });

  }

}

class MyPage {
  @ViewChild(Slides) slides: Slides;

  goToSlide() {
    this.slides.slideTo(2, 500);
  }
}
